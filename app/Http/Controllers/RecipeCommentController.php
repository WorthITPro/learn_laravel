<?php

namespace App\Http\Controllers;

use App\Http\Requests\Recipes\Comments\DeleteRequest;
use App\Recipe;
use App\RecipeComment;
use Illuminate\Http\Request;

class RecipeCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $recipeId)
    {
        $recipe = Recipe::find($recipeId);

        // Notice, we aren't specifying the RECIPE ID... because this relationship does that for us...
        $recipe->comments()->create([
            RecipeComment::ATTR_AUTHOR_ID => auth()->user()->getId(),
            RecipeComment::ATTR_BODY => request('body')
        ]);

        // When we are done... go back to the same view..
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // NOW THIS LOOKS RIGHT... BUT ITS WRONG!!!!
    // WATCH THIS....

    // WHOA! I just deleted a comment that WAS NOT MINE!!!! WHAT?????

    // This is where FORM requests come in...

    // Now let's attach our new DeleteRequest (Requests, are ALWAYS first method parameter...)

    // Notice, we aren't going to DO anything with it... we are using it as a GUARD...
    public function destroy(DeleteRequest $request, $recipeId, $commentId)
    {
        RecipeComment::destroy($commentId);

        return redirect()->back();
    }
}
