<?php

namespace App\Http\Requests\Recipes\Comments;

use App\RecipeComment;
use Illuminate\Foundation\Http\FormRequest;

class DeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // We ONLY want to authorize this action if the authenticated user is the comment author...

        // I am authorized to delete my OWN comments...

        // but if I try to delete someone elses by modifying the code...


        // NOW the form request has prevented me from doing an action... so 

        // hiding the button on the view was great, but didn't prevent me from doing it...

        // using the Form Request prevented the action at the server level...

        $comment = RecipeComment::find($this->route('comment')); // $this->route() gets it from the URL for comment ID... we use 'comment' because our resource is using 'comments' plural :D

        return auth()->user()->getId() == $comment->getAuthor()->getId(); // We turn TRUE if they equal, else return false...
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
