<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    // Some of the things I do here aren't necessary, but they make for easier updating when things change

    // We need to setup the inverse relationships here...

    const DB_TABLE = 'recipes';

    const ATTR_ID = 'id';
    const ATTR_NAME = 'name';
    const ATTR_DESCRIPTION = 'description';

    protected $primaryKey = self::ATTR_ID;

    protected $fillable = [
    	self::ATTR_NAME,
    	self::ATTR_DESCRIPTION
    ];
    
    public function getId()
    {
    	return $this->{self::ATTR_ID};
    }

    public function getName()
    {
    	return $this->{self::ATTR_NAME};
    }

    public function getDescription()
    {
    	return $this->{self::ATTR_DESCRIPTION};
    }

    public function getComments()
    {
        return $this->comments;
    }

    public function comments()
    {
        return $this->hasMany(RecipeComment::class, RecipeComment::ATTR_RECIPE_ID); // Has Many Comment Class via 'recipe_id'
    }
}
