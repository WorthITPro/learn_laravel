<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeComment extends Model
{
    // What information do we need for a comment? we want to know the user and what they said?

    // And we need to know what recipe it was on... :D

    const DB_TABLE = 'recipe_comments';

    const ATTR_ID = 'id';
    const ATTR_AUTHOR_ID = 'author_id';
    const ATTR_RECIPE_ID = 'recipe_id';
    const ATTR_BODY = 'body';

    protected $table = self::DB_TABLE;

    protected $fillable = [
    	self::ATTR_AUTHOR_ID,
    	self::ATTR_BODY
    ];

    public function getId()
    {
    	return $this->{self::ATTR_ID};
    }

    public function getBody()
    {
    	return $this->{self::ATTR_BODY};
    }

    // I use getters to my eloquent relationships but you could say $comment->recipe->getId() ...
    // I prefer... $comment->getRecipe()->getId() // Here, I know EXACTLY what's happening...
    public function getRecipe()
    {
    	return $this->recipe;
    }

    public function getAuthor()
    {
    	return $this->author;
    }

    public function isOwner()
    {  
        // Does the authenticated users id match the comment's author_id ???

        // BUT..... That doesn't mean it's "SAFE GUARDED".... will show why...
        return auth()->user()->getId() == $this->getAuthor()->getId();
    }

    // Now we need to setup the relationships.. we have 2 relationship...

    // Recipe and Author (aka User)

    public function recipe()
    {
    	return $this->belongsTo(Recipe::class, self::ATTR_RECIPE_ID); // Belongs to the Recipe Class via 'recipe_id'
    }

    public function author()
    {
    	return $this->belongsTo(User::class, self::ATTR_AUTHOR_ID); // Belongs to User Class vie 'author_id';
    }
}
