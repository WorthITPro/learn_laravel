<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const DB_TABLE = 'users';

    const ATTR_ID = 'id';
    const ATTR_NAME = 'name';
    const ATTR_EMAIL = 'email';
    const ATTR_PASSWORD = 'password';
    const ATTR_REMEMBER_TOKEN = 'remember_token';
    const ATTR_EMAIL_VERIFIED_AT = 'email_verified_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::ATTR_NAME,
        self::ATTR_EMAIL,
        self::ATTR_PASSWORD
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::ATTR_PASSWORD,
        self::ATTR_REMEMBER_TOKEN
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::ATTR_EMAIL_VERIFIED_AT => 'datetime',
    ];

    protected $table = self::DB_TABLE;

    public function getId()
    {
        return $this->{self::ATTR_ID};
    }

    public function getName()
    {
        return $this->{self::ATTR_NAME};
    }

    // Again, I like to use GET
    public function getComments()
    {
        return $this->comments;
    }

    public function comments()
    {
        return $this->hasMany(RecipeComment::class, RecipeComment::ATTR_AUTHOR_ID); // has many comment class via 'author_id'
    }
}
