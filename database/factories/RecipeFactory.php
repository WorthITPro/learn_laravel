<?php

use App\Recipe;
use Faker\Generator as Faker;

$factory->define(Recipe::class, function (Faker $faker) {
    return [
        Recipe::ATTR_NAME => $faker->name, // Fake Name
        Recipe::ATTR_DESCRIPTION => $faker->sentence(10) // Fake Sentence 
    ];
});
