<?php

use App\Recipe;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create(Recipe::DB_TABLE, function (Blueprint $table) {
            
            $table->increments(Recipe::ATTR_ID);
            
            $table->string(Recipe::ATTR_NAME);

            $table->text(Recipe::ATTR_DESCRIPTION);

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Recipe::DB_TABLE);
    }
}
