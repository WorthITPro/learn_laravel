<?php

use App\RecipeComment;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecipeCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(RecipeComment::DB_TABLE, function (Blueprint $table) {
            
            $table->increments(RecipeComment::ATTR_ID);

            $table->integer(RecipeComment::ATTR_AUTHOR_ID); // User id who made comment

            $table->integer(RecipeComment::ATTR_RECIPE_ID); // Recipe ID comment is attached to...

            $table->text(RecipeComment::ATTR_BODY);

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(RecipeComment::DB_TABLE);
    }
}
