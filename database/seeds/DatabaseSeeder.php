<?php

use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	// Let's make some recipes...

        // Also we should refresh the DB automatically when we run a seeder 

    	// We can also throw output to the console

    	system('clear'); // Clear the console...

    	$this->command->line('Refresing Database...');

        \Artisan::call('migrate:refresh');

        $this->command->info('Database Refreshed!!!');

        $this->call(RecipeTableSeeder::class);

        factory(User::class, 3)->create();
    }
}
