<?php

use App\Recipe;
use Illuminate\Database\Seeder;

class RecipeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->line('Seeding Recipes...');

        factory(Recipe::class, 20)->create(); // 20 should be good right?

        $this->command->info('Recipes Seeded');

        // Now we have 20 recipes to play with...

        // we need a controller / route now...
    }
}
