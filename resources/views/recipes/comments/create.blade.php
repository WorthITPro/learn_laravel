<div class="new_comment">

	<p>Commenting As: {{ auth()->user()->getName() }}</p>

	<form method="POST" action="{{ route('recipes.comments.store', $recipe->getId()) }}">

		<!-- I never use php and I use ajax / vue -->
		{{ csrf_field() }}

		<textarea name="body"></textarea>

		<input type="submit" class="btn btn-primary" value="Add Comment" />

	</form>

</div>