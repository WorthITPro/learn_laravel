<!-- Now we need to list comments -->

<!-- Now you're probably saying... WHOA WTF!!! None of that exists... 

	You're absolutely right...
-->

<!-- NO ERRORS NOW... yay... but what if there are no comments??? -->


<!-- Now the question was, in regarding to deleting your OWN comments -->

<!-- So with this, we will need a way to "switch" users -->

<div class="comment_list">

	@if($recipe->getComments()->count()) <!-- IF we have comments -->

		@foreach($recipe->getComments() as $comment) <!-- SHOW THEM -->

		<div class="comment">

			<h2>{{ $comment->getAuthor()->name }} says...</h2>
			<p>{{ $comment->getBody() }}</p>

			<!-- We don't want to show this button for EVERYONE... -->
			<!-- Only for the User who wrote it... -->
			@if ($comment->isOwner()) <!-- Create new method on the COMMENT class -->
				<form action="{{ route('recipes.comments.destroy', [ 'recipe' => $recipe->getId(), 'comment' => $comment->getId() ]) }}" method="POST">
					{{ csrf_field() }}
					@method('DELETE')
					<button class="btn btn-sm btn-danger">Delete Comment</button>					
				</form>
			@endif

		</div>

		@endforeach

	@else

		<p>There are no comments... BE THE FIRST!</p>

	@endif

</div>