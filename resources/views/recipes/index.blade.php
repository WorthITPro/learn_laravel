@extends('template')

@section('content')

	<h1>List of Recipes</h1>

	<table class="table" role="table">

		<thead>
			<tr>
				<th>Recipe Name</th>
				<th>Recipe Description</th>
			</tr>
		</thead>

		<tbody>
			@foreach($recipes as $recipe)
			<tr>
				<td>
					<a href="{{ route('recipes.show', $recipe->getId()) }}">{{ $recipe->getName() }}</a>
				</td>
				<td>{{ $recipe->getDescription() }}</td>
			</tr>
			@endforeach	
		</tbody>

	</table>

@endsection