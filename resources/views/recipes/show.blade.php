@extends('template')

@section('content')

	<h1>Here's a Recipe</h1>

	<a href="{{ route('recipes.index') }}">Back to Recipe List</a>

	<h2>{{ $recipe->getName() }}</h2>

	<p>{{ $recipe->getDescription() }}</p>

	<!-- Let's bring our comment create form INTO our show view...
		This will let $recipe be allowed inside the Comment Form -->

	@include('recipes.comments.create')

	<!-- Let's bring our list of comments into the view as well ... -->
	@include('recipes.comments.index')

	<!-- Now we are back here, no errors... meaning so far so good... -->

	<!-- Think it's goint to error???? -->
	<!-- ABSOLUTELY !!!! -->

@endsection