<?php

use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('template');
});

// SPECIAL ROUTE JUST FOR DEMO
Route::get('/switch/{user}', [ 'as' => 'switch_user', function($user) {

	// We don't need a controller for this...
	auth()->loginUsingId($user);
	return redirect()->back();

} ]);

// For this demo, we are going to need a user, and pretend to be authenticated (althoug normally we would use login...)

// so I'm going to seed a temp user and then just login as him ALL the time :D

// THIS IS WHERE RESOURCES SHINE!!!!!

Route::resource('recipes', 'RecipeController'); // Using resource gives us ALL crud routes :D LOVE IT, USE IT... It's amazing!


// We can daisy chain resources together by a . prefix...

Route::resource('recipes.comments', 'RecipeCommentController'); // and let's create the controller